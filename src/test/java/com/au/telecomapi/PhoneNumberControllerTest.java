package com.au.telecomapi;

import static java.lang.String.format;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.au.telecomapi.data.PhoneNumber;
import com.au.telecomapi.data.PhoneNumberStatus;
import com.au.telecomapi.data.repository.PhoneNumberRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/test.properties")
public class PhoneNumberControllerTest {

  @Autowired private MockMvc mockMvc;
  @Autowired private PhoneNumberRepository phoneNumberRepository;

  @BeforeEach
  public void setUp() {
    phoneNumberRepository.deleteAll();
  }

  @Test
  public void shouldReturnAnEmptyListOfPhoneNumbers() throws Exception {
    // No phone numbers loaded in database
    mockMvc
        .perform(get("/api/phone-numbers"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  public void shouldReturnPhoneNumbers() throws Exception {
    phoneNumberRepository.save(new PhoneNumber("+6148888001", PhoneNumberStatus.ACTIVE, null));
    phoneNumberRepository.save(new PhoneNumber("+6148888002", PhoneNumberStatus.DEACTIVATED, null));
    mockMvc
        .perform(get("/api/phone-numbers"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].number", is("+6148888001")))
        .andExpect(jsonPath("$[0].status", is("ACTIVE")))
        .andExpect(jsonPath("$[1].number", is("+6148888002")))
        .andExpect(jsonPath("$[1].status", is("DEACTIVATED")));
  }

  @Test
  public void shouldActivatePhoneNumber() throws Exception {
    PhoneNumber phoneNumber =
        phoneNumberRepository.save(
            new PhoneNumber("+6148888001", PhoneNumberStatus.DEACTIVATED, null));
    mockMvc
        .perform(put(format("/api/phone-numbers/%s/activate", phoneNumber.getNumber())))
        .andExpect(status().isOk());
    PhoneNumber refreshedPhoneNumber =
        phoneNumberRepository.findById(phoneNumber.getNumber()).get();
    assertEquals(PhoneNumberStatus.ACTIVE, refreshedPhoneNumber.getStatus());
  }

  @Test
  public void shouldNotActivateInvalidNumber() throws Exception {
    mockMvc
        .perform(put(format("/api/phone-numbers/%s/activate", "invalidNumber")))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void shouldDeactivatePhoneNumber() throws Exception {
    PhoneNumber phoneNumber =
        phoneNumberRepository.save(new PhoneNumber("+6148888001", PhoneNumberStatus.ACTIVE, null));
    mockMvc
        .perform(put(format("/api/phone-numbers/%s/deactivate", phoneNumber.getNumber())))
        .andExpect(status().isOk());
    PhoneNumber refreshedPhoneNumber =
        phoneNumberRepository.findById(phoneNumber.getNumber()).get();
    assertEquals(PhoneNumberStatus.DEACTIVATED, refreshedPhoneNumber.getStatus());
  }

  @Test
  public void shouldNotDeactivateInvalidNumber() throws Exception {
    mockMvc
        .perform(put(format("/api/phone-numbers/%s/deactivate", "invalidNumber")))
        .andExpect(status().is4xxClientError());
  }
}
