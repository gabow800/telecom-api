package com.au.telecomapi;

import static java.lang.String.format;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.au.telecomapi.data.PhoneNumber;
import com.au.telecomapi.data.PhoneNumberStatus;
import com.au.telecomapi.data.User;
import com.au.telecomapi.data.repository.PhoneNumberRepository;
import com.au.telecomapi.data.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/test.properties")
public class UserControllerTest {

  @Autowired private MockMvc mockMvc;
  @Autowired private UserRepository userRepository;
  @Autowired private PhoneNumberRepository phoneNumberRepository;

  @BeforeEach
  public void setUp() {
    phoneNumberRepository.deleteAll();
    userRepository.deleteAll();
  }

  @Test
  public void shouldReturnAnEmptyListOfUsers() throws Exception {
    // No users loaded in database
    mockMvc
        .perform(get("/api/users"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  public void shouldReturnUsers() throws Exception {
    userRepository.save(new User("Marshall", "Erickson"));
    userRepository.save(new User("Ted", "Mosby"));
    mockMvc
        .perform(get("/api/users"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].firstName", is("Marshall")))
        .andExpect(jsonPath("$[0].lastName", is("Erickson")))
        .andExpect(jsonPath("$[1].firstName", is("Ted")))
        .andExpect(jsonPath("$[1].lastName", is("Mosby")));
  }

  @Test
  public void shouldReturnUserAndPhoneNumbers() throws Exception {
    User user = userRepository.save(new User("Barney", "Stinson"));
    phoneNumberRepository.save(new PhoneNumber("+61488880001", PhoneNumberStatus.ACTIVE, user));
    phoneNumberRepository.save(
        new PhoneNumber("+61488880002", PhoneNumberStatus.DEACTIVATED, user));
    mockMvc
        .perform(get(format("/api/users/%d", user.getId())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.firstName", is("Barney")))
        .andExpect(jsonPath("$.lastName", is("Stinson")))
        .andExpect(jsonPath("$.numbers", hasSize(2)))
        .andExpect(jsonPath("$.numbers[0].number", is("+61488880001")))
        .andExpect(jsonPath("$.numbers[0].status", is("ACTIVE")))
        .andExpect(jsonPath("$.numbers[1].number", is("+61488880002")))
        .andExpect(jsonPath("$.numbers[1].status", is("DEACTIVATED")));
  }

  @Test
  public void shouldErrorOutWithInvalidUser() throws Exception {
    Long invalidUserId = 1200L;
    mockMvc
        .perform(get(format("/api/users/%d", invalidUserId)))
        .andExpect(status().is4xxClientError());
  }
}
