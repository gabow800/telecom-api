package com.au.telecomapi.service;

import com.au.telecomapi.data.PhoneNumber;
import com.au.telecomapi.data.PhoneNumberStatus;
import com.au.telecomapi.data.repository.PhoneNumberRepository;
import com.au.telecomapi.exception.PhoneNumberNotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneNumberService {

  @Autowired private PhoneNumberRepository phoneNumberRepository;

  public List<PhoneNumber> getAllPhoneNumbers() {
    return phoneNumberRepository.findAll();
  }

  public PhoneNumber getPhoneNumber(String number) {
    return phoneNumberRepository
        .findById(number)
        .orElseThrow(() -> new PhoneNumberNotFoundException(number));
  }

  public PhoneNumber activatePhoneNumber(PhoneNumber phoneNumber) {
    phoneNumber.setStatus(PhoneNumberStatus.ACTIVE);
    return phoneNumberRepository.save(phoneNumber);
  }

  public PhoneNumber deactivatePhoneNumber(PhoneNumber phoneNumber) {
    phoneNumber.setStatus(PhoneNumberStatus.DEACTIVATED);
    return phoneNumberRepository.save(phoneNumber);
  }
}
