package com.au.telecomapi.exception;

import static java.lang.String.format;

public class PhoneNumberNotFoundException extends RuntimeException {
  public PhoneNumberNotFoundException(String number) {
    super(format("Phone number %s not found", number));
  }
}
