package com.au.telecomapi.controller;

import com.au.telecomapi.data.User;
import com.au.telecomapi.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {

  @Autowired private UserService userService;

  @GetMapping("")
  List<User> getAllUsers() {
    return userService.getAllUsers();
  }

  @GetMapping("/{id}")
  User getUser(@PathVariable Long id) {
    return userService.getUser(id);
  }
}
