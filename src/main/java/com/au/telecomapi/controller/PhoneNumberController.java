package com.au.telecomapi.controller;

import com.au.telecomapi.data.PhoneNumber;
import com.au.telecomapi.service.PhoneNumberService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/phone-numbers")
public class PhoneNumberController {

  @Autowired private PhoneNumberService phoneNumberService;

  @GetMapping("")
  List<PhoneNumber> getAllPhoneNumbers() {
    return phoneNumberService.getAllPhoneNumbers();
  }

  @GetMapping("/{number}")
  PhoneNumber getPhoneNumber(@PathVariable String number) {
    return phoneNumberService.getPhoneNumber(number);
  }

  @PutMapping("/{number}/activate")
  PhoneNumber activatePhoneNumber(@PathVariable String number) {
    PhoneNumber phoneNumber = phoneNumberService.getPhoneNumber(number);
    return phoneNumberService.activatePhoneNumber(phoneNumber);
  }

  @PutMapping("/{number}/deactivate")
  PhoneNumber deactivatePhoneNumber(@PathVariable String number) {
    PhoneNumber phoneNumber = phoneNumberService.getPhoneNumber(number);
    return phoneNumberService.deactivatePhoneNumber(phoneNumber);
  }
}
