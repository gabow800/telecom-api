package com.au.telecomapi.data;

public enum PhoneNumberStatus {
  CLAIMED, // Claimed by other carrier
  ACTIVE, // Active customer
  DEACTIVATED, // Number owned by us but not active
}
