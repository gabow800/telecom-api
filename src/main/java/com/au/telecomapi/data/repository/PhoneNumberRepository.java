package com.au.telecomapi.data.repository;

import com.au.telecomapi.data.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, String> {}
