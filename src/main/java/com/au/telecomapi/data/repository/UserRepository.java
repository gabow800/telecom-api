package com.au.telecomapi.data.repository;

import com.au.telecomapi.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {}
