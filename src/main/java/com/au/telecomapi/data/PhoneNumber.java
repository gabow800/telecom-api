package com.au.telecomapi.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PhoneNumber {
  private @Id String number;
  private PhoneNumberStatus status;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public PhoneNumber() {}

  public PhoneNumber(String number, PhoneNumberStatus status, User user) {
    this.number = number;
    this.status = status;
    this.user = user;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public PhoneNumberStatus getStatus() {
    return status;
  }

  public void setStatus(PhoneNumberStatus status) {
    this.status = status;
  }

  @JsonIgnore
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
