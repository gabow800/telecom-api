package com.au.telecomapi;

import com.au.telecomapi.data.PhoneNumber;
import com.au.telecomapi.data.PhoneNumberStatus;
import com.au.telecomapi.data.User;
import com.au.telecomapi.data.repository.PhoneNumberRepository;
import com.au.telecomapi.data.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "loadDatabase", havingValue = "true")
public class LoadDatabase {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Autowired private PhoneNumberRepository phoneNumberRepository;

  @Autowired private UserRepository userRepository;

  @Bean
  CommandLineRunner initializeDatabase() {
    return args -> {
      log.info("Loading database...");
      User rob = userRepository.save(new User("Rob", "Manning"));
      phoneNumberRepository.save(
          new PhoneNumber("+61488884441", PhoneNumberStatus.DEACTIVATED, rob));
      User sarah = userRepository.save(new User("Sarah", "Aspen"));
      phoneNumberRepository.save(new PhoneNumber("+61488884442", PhoneNumberStatus.ACTIVE, sarah));
      log.info("--------------------------");
      log.info("Finished loading database.");
    };
  }
}
