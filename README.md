# Telecom API

Welcome to the project, this is a REST API that exposes information about phone numbers and users, it is built
using Java and Spring Boot.

## Pre-requisites
- Java 17

## Setup
Clone the repository.
```bash
git clone https://gitlab.com/gabow800/telecom-api.git
```

Cd into the repository and build the project.
```bash
cd telecom-api/
./gradlew build
```

Start the server on [http://localhost:8080/]().
```bash
./gradlew bootRun
```

## Using the App

The app exposes a few REST endpoints that you can invoke locally. The data is already preloaded in an H2 database
that persists the data in memory only, so every time the server is restarted the data gets deleted and preloaded
again.

Get available phone numbers:
```bash
curl --location --request GET 'http://localhost:8080/api/phone-numbers'
```

Get all phone numbers of a user:
```bash
curl --location --request GET 'http://localhost:8080/api/users/1'
```

Activate a phone number:
```bash
curl --location --request PUT 'http://localhost:8080/api/phone-numbers/+61488884441/activate'
```

## Documentation

This project uses the OpenAPI plugin for generating the REST API documentation automatically based on the
Spring Controllers available. In order to read the docs start the server and access [http://localhost:8080/docs/ui.html]().

## Preloaded data

If you want to change the preloaded data then you can go to the [LoadDatabase](src/main/java/com/au/telecomapi/LoadDatabase.java) class
and update the `initializeDatabase()` script.

## Tests
Currently there are only functional tests and they test the Spring Controllers interacting with the database with a good
coverage of all the scenarios. In the future unit tests could be introduced when the business logic gets more complex
and the Spring Services get bigger.

## Useful commands

```bash
# Run tests
./gradlew test

# Check coverage
# Open build/reports/jacoco/test/html/index.html after running command
./gradlew check

# Format code
./gradlew spotlessApply
```